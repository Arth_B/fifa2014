working_directory "/var/ror/fifa2014-prod"
pid "/var/ror/fifa2014-prod/tmp/pids/unicorn-3.pid"
stderr_path "/var/ror/fifa2014-prod/log/unicorn.log"
stdout_path "/var/ror/fifa2014-prod/log/unicorn.log"

listen "/tmp/unicorn.fifa2014-prod-3.sock"
worker_processes 4
timeout 30
