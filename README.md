 ## Le portail

_Préambule_
Ce portail a pour but de faire des pronostiques pour la FIFA 2014.
- Nous utilisons l'API suivante pour récupérer les données du mondial : `https://github.com/openfootball/openfootball.github.io/blob/master/talks/football_db_intro.md`
- Ce portail a aussi sa propre API REST pour l'authentification (simple), les pronostiques, les classements, ...
- Ce portail utilise une BDD MySQL

1. Les vues

On utilise un template général pour le portail : `app/views/layouts`
Celui-ci importe les CSS et les librairies JS dans le <head>
Le fichier de script JS personalisé est mis à la fin du body.

On a 3 vues utilisateurs
- accueil (qui comporte une section de connection)
- mes pronostiques : lieu où on retrouve ses pronostiques et où on les effectue aussi
- le classement : classement des participants selon des RG

On a 0 vues admin (tout se synchronisera via l'open data de la fifa)

2. L'authentification / Inscription

Sur la page d'accueil on a un formulaire de connexion le fonctionnement est le suivant : 
- Si on se connecte pour la première fois le compte user est crée
- Si on s'était déjà connecté, les données saisies vont être comparées avec l'utilisateur qui aura le même prénom + nom

Dans le cas
- d'une réusite on est redirigé vers "mes pronostiques"
- d'un échec on est redirigé vers la page d'accueil

Un message `flash` s'affiche en cas d'erreur ou de création de compte


3. Mes pronostiques

Les pronostiques sont affichés dans des tableaux selon les jours et "Phases" :
- Phase de groupe
- Deuxième phase 

4. Synchronizer

/!\ Je suis en train de faire une appli qui synchronisera en temps quasi réél les resultats. L'appli openfootball ne garantissait pas des resultats "en live".
```
OLD actions
On utilise une classe `synchronizer` dans l'espace `app/domain/synchroniser.rb` pour récupérer les données de l'API openfootball

Des tâches rake de synchronisation sont disponibles :
- `rake synchronize:matchdays` : crée ou update selon les données reçues les "jours de match" dans notre BDD
```
 
 
    ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Welcome to your Rails app on Cloud9 IDE!

Check out the following guide points to get started with your Rails application:

1) To run your application, type the following in the terminal:

  $ rails s -b $IP -p $PORT

2) To use MySQL instead of sqlite for persistence, check out https://docs.c9.io/running_a_rails_app.html#use-mysql-in-your-rails-app

3) If you want, pick the older Ruby version 1.8.7 using the 'rvm' command in the terminal.

4) If you want, pick the rails version you like installing it through the terminal:

  $ gem install rails --version 4.0.0

5) Heroku CLI
    wget http://assets.heroku.com/heroku-client/heroku-client.tgz
    tar xzfv heroku-client.tgz

    cd heroku-client/bin
    PATH=$PATH:$PWD
    cd ~/885490/

6) import CSV dump

```
# Users
require 'csv'
ActiveRecord::Base.connection.execute("TRUNCATE TABLE users")
csv_text = File.read('users.csv')
csv = CSV.parse(csv_text, :headers => true)
csv.each do |row|
  User.create!(row.to_hash)
end
```
```
# Pronostics
require 'csv'
ActiveRecord::Base.connection.execute("TRUNCATE TABLE pronostics")
csv_text = File.read('pronostics.csv')
csv = CSV.parse(csv_text, :headers => true)
csv.each do |row|
  Pronostic.create!(row.to_hash)
end
```
```
# Pronostics with kicks at goal
require 'csv'
ActiveRecord::Base.connection.execute("TRUNCATE TABLE pronostic_with_kicks_at_goals")
csv_text = File.read('pronostic_with_kicks_at_goals.csv')
csv = CSV.parse(csv_text, :headers => true)
csv.each do |row|
  PronosticWithKicksAtGoal.create!(row.to_hash)
end
```
```
# Games
require 'csv'
ActiveRecord::Base.connection.execute("TRUNCATE TABLE games")
csv_text = File.read('games.csv')
csv = CSV.parse(csv_text, :headers => true)
csv.each do |row|
  Game.create!(row.to_hash)
end
```

```
kill -9 PID
```

## Support & Documentation

Visit http://docs.c9.io for documentation, or http://support.c9.io for support. 
To watch some training videos, visit http://www.youtube.co