class Game < ActiveRecord::Base
    has_many :pronostics

    def team_win
        if self.team_a_score == self.team_b_score
            team_win = "nul"
        elsif self.team_a_score > self.team_b_score
            team_win = "a"
        else
            team_win = "b"
        end
        team_win
    end
    
    def teams
        teams = {"a" => self.team_a_country, "b" => self.team_b_country }
    end
end
