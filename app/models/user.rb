class User < ActiveRecord::Base
    has_many :pronostics
    has_many :pronostic_with_kicks_at_goals
    has_many :comments
end
