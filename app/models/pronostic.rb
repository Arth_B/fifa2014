class Pronostic < ActiveRecord::Base
    belongs_to :user
    belongs_to :game
    has_one :pronostic_with_kicks_at_goal

    def team_win
        if self.team_a_score == self.team_b_score
            team_win = "nul"
        elsif self.team_a_score > self.team_b_score
            team_win = "a"
        else
            team_win = "b"
        end
        team_win
    end
end
