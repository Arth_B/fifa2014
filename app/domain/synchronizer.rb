class Synchronizer
    require 'json'
    require 'net/http'
    def store_score
        #pronostics = Pronostic.all
        #pronostics.each do |pronostic|
        #    pronostic.update_attributes({:score=>nil})
        #    puts "reset #{pronostic.id}"
        #end
        pronostics = Pronostic.where({:score => nil})
        puts ">> PRONOSTICS : (x#{pronostics.count})"
        pronostics.each do |pronostic|
            puts ">>     pronostic #{pronostic.id}"
            game = Game.find pronostic.game_id

            puts "          Is game ended ?"
            if game.ended==true
                puts "              YES"
                
                points_result = get_points_result pronostic, game
                points_score = get_points_score pronostic, game
                points_kick_at_goal = get_points_kick_at_goal pronostic, game
                points = points_result + points_score + points_kick_at_goal
                
                puts "                  pts result : "+points_result.to_s
                puts "                  pts score : "+points_score.to_s
                puts "                  pts k. at g. : "+points_kick_at_goal.to_s
                puts "                      = "+points.to_s
                
                pronostic.update_attributes({:score => points, :pts_result=>points_result, :pts_score=>points_score, :pts_kick_at_goal=>points_kick_at_goal})
            else
                puts "              NO"
            end
        end
    end

    def store_and_update_user_score_and_rank        
        users = User.all
        users.each do |user|
            puts ">> User : "+user.first_name
            user_pronostics = user.pronostics
            bad_pronostics = user_pronostics.where('score IS NOT NULL').where(:score => 0).count
            good_pronostics = user_pronostics.where('score IS NOT NULL').where('score > 0').count
            all_pronostics = user_pronostics.where('score IS NOT NULL').count
            puts "  >> all_pronostics"+bad_pronostics.to_s
            puts "  >> good_pronostics / bad_pronostics"+good_pronostics.to_s+" / "+bad_pronostics.to_s

            user_score = user_pronostics.where(:score!=nil).sum :score
            if all_pronostics==0
                ratio_good_pronostics = 0
            else
                ratio_good_pronostics = (good_pronostics.to_f * 100.to_f / all_pronostics.to_f).round 2
            end
            puts "  >> ratio_good_pronostics"+ratio_good_pronostics.to_s
            user.update_attributes({:score=>user_score, :ratio_good_pronostics=>ratio_good_pronostics})
        end
        users = User.order('score DESC, ratio_good_pronostics DESC')
        user_i = 1
        users.each do |user|
            user.update_attributes({:rank=>user_i})
            user_i = user_i+1
        end
    end

    def get_rounds_and_games_with_kicks_at_goal
        api_fifa_rounds = "http://fifa-synch.herokuapp.com/api/matchdays"
        response = Net::HTTP.get_response(URI.parse(api_fifa_rounds))
        data = response.body
        json_data = JSON.parse(data)
        return json_data
    end

    def getGames matchday
        open_data_FIFA_games_per_round = "http://footballdb.herokuapp.com/api/v1/event/world.2014/round/#{matchday.pos}"
        response = Net::HTTP.get_response(URI.parse(open_data_FIFA_games_per_round))
        data = response.body
        json_data = JSON.parse(data)
        return json_data["games"]
    end

    def store_and_update_games
        api_fifa_games = 'http://fifa-synch.herokuapp.com/api/games'
        response = Net::HTTP.get_response(URI.parse(api_fifa_games))
        data = response.body
        json_data = JSON.parse(data)
        games = json_data["games"]
        puts ">> GAMES"
        games.each do |game|
            if game!=false
                game_id = game['id']
                existing_game = Game.find_by_id game_id
                puts "    Game #{game_id} exists ?"
                if existing_game.nil?
                    game_team_a = game['team_a']
                    game_team_b = game['team_b']
                    puts "        CREATING"
                    cet_game_datetime = (DateTime.parse(game['datetime']) + 2.hours).strftime('%a, %d %b %Y %H:%M:%S')
                    Game.create({:id=>game_id, :ended=>false, :datetime=>cet_game_datetime, :team_a_country=>game_team_a['country'], :team_a_flag=>game_team_a['flag'], :team_a_score=>game_team_a['score'], :team_b_country=>game_team_b['country'], :team_b_flag=>game_team_b['flag'], :team_b_score=>game_team_b['score']})
                else
                    game_team_a = game['team_a']
                    game_team_b = game['team_b']
                    cet_game_datetime = (DateTime.parse(game['datetime']) + 2.hours).strftime('%a, %d %b %Y %H:%M:%S')

                    puts "            #{cet_game_datetime}"
                    puts "            #{existing_game.datetime.strftime('%a, %d %b %Y %H:%M:%S')}"

            	    if existing_game.datetime.nil?
            	        puts "            UPDATED time"
            	        existing_game.update_attributes({:datetime=>cet_game_datetime})
            	    elsif existing_game.datetime.strftime('%a, %d %b %Y %H:%M:%S')!=cet_game_datetime
            	        puts "            UPDATED time"
            	        existing_game.update_attributes({:datetime=>cet_game_datetime})
            	    else
            	        puts "            NO changes time"
            	    end

            	    if existing_game.ended!=game['ended']
            	        existing_game.update_attributes({:ended=>game['ended']})
                        puts "            UPDATING ended"
            	    else
            	        puts "            NO ended changes"
            	    end

                    if existing_game.team_win!=game['team_win']
                        existing_game.update_attributes({:team_win_kick_at_goal=>game['team_win']})
                        puts "            UPDATING team_win_kick_at_goal"
                    else
                        puts "            NO team_win changes"
                    end

                    if((existing_game.team_a_score!=game_team_a['score']) && (existing_game.team_b_score!=game_team_b['score']))
                        existing_game.update_attributes({:team_a_score=>game_team_a['score'], :team_b_score=>game_team_b['score']})
                        puts "            UPDATING t_a_score t_b_score"
                    elsif((existing_game.team_a_score!=game_team_a['score']) && (existing_game.team_b_score==game_team_b['score']))
                        existing_game.update_attributes({:team_a_score=>game_team_a['score']})
                        puts "            UPDATING t_a_score"
                    elsif((existing_game.team_a_score==game_team_a['score']) && (existing_game.team_b_score!=game_team_b['score']))
                        existing_game.update_attributes({:team_b_score=>game_team_b['score']})
                        puts "            UPDATING t_b_score"
                    else
                        puts "            NO score changes"
                    end
                end
            else
                puts "    No game"
            end
        end
    end
    
    
    private
    
    def get_points_result pronostic, game
        p_a_score = pronostic.team_a_score
        p_b_score = pronostic.team_b_score
        g_a_score = game.team_a_score
        g_b_score = game.team_b_score
        team_win_kick_at_goal = game.team_win_kick_at_goal
        
        if p_a_score==p_b_score && g_a_score==g_b_score # nul
            points_result = 1
        elsif p_a_score<p_b_score && g_a_score<g_b_score # B win
            points_result = 1
        elsif p_a_score>p_b_score && g_a_score>g_b_score # A win
            points_result = 1
        elsif team_win_kick_at_goal=="a" && p_a_score>p_b_score # A win with kicks at goal
            points_result = 1
        elsif team_win_kick_at_goal=="b" && p_a_score<p_b_score # A win with kicks at goal
            points_result = 1
        else
            points_result = 0
        end
    end
    def get_points_score pronostic, game
        p_a_score = pronostic.team_a_score
        p_b_score = pronostic.team_b_score
        g_a_score = game.team_a_score
        g_b_score = game.team_b_score
        
        if p_a_score==g_a_score && p_b_score==g_b_score
            points_score = 2
        else
            points_score = 0
        end
    end
    def get_points_kick_at_goal pronostic, game
        pronostic_with_kicks_at_goal = pronostic.pronostic_with_kicks_at_goal
        if !pronostic_with_kicks_at_goal.nil?
            if !game.team_win_kick_at_goal.nil?
                if pronostic_with_kicks_at_goal.team_win = game.team_win_kick_at_goal
                    points_result = 1
                else
                    points_result = 0
                end
            else
                points_result = 0
            end
        else
            points_result = 0
        end
    end
end