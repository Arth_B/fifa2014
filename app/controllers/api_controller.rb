class ApiController < ActionController::Base
    # Prevent CSRF attacks by raising an exception.
    # For APIs, you may want to use :null_session instead.
    protect_from_forgery with: :exception
    
    def authentificated!
        if session[:user]
            return true	
        else
            render :json => {:status=>403, :error=>"User not authenticated"}, :status => :forbidden
            return false
        end
    end
end
