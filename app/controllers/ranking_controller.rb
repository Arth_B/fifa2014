class RankingController < ApplicationController
    def index
        get_session
		@title_page = "Le classsement - FIFA World Cup 2K14"
		
		@users = User.all.order('rank ASC')
		unless @current_user.nil?
    		user_pronostics = User.find(@current_user[:id]).pronostics
    		@user_pronostics_games = []
    		user_pronostics.each do |pronostic|
    		    @user_pronostics_games << pronostic.game_id
    		end
    	end
	end
end