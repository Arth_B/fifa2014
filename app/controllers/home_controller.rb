class HomeController < ApplicationController
    def index
		@title_page = "Accueil - FIFA World Cup 2K14"
		if is_signed_in?
		    redirect_to :mes_pronostiques
		end
	end
end
