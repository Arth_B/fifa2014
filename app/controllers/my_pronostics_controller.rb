class MyPronosticsController < ApplicationController
    before_filter :authentificated!

    def index
        sync = Synchronizer.new
		synch_data = sync.get_rounds_and_games_with_kicks_at_goal
		@rounds = synch_data["rounds"]
		@games_with_kicks_at_goal = synch_data["gamesWithKicksAtGoal"]
		@title_page = "Mes pronostics - FIFA World Cup 2K14"
	end

	def create
	    new_pronostic_params = params.required :pronostic
		new_pronostic_params.permit!

		new_pronostic_params[:user_id] = session[:user][:id]

		existing_pronostic = Pronostic.where({:user_id=>session[:user][:id], :game_id=>new_pronostic_params[:game_id]})

		game = Game.find new_pronostic_params[:game_id]
		if isAbleToCommitPronostic? game
    		if new_pronostic_params[:team_a_score]=="" || new_pronostic_params[:team_b_score]==""
    		    flash[:error] = "Saisissez des pronostics pour les 2 equipes"
    		else
        		if existing_pronostic.count == 0
        		    if new_pronostic_params[:team_a_score]==new_pronostic_params[:team_b_score]
        		    	if params[:kick_at_goal_winner]=="a" || params[:kick_at_goal_winner]=="b"
        		    		new_pronostic = Pronostic.new(new_pronostic_params)
            	    		pronostic_saved = new_pronostic.save
        		    		pronostic_id = new_pronostic.id
        		    		current_user = User.find session[:user][:id]
        		    		pronostic_with_kicks_at_goals_saved = current_user.pronostic_with_kicks_at_goals.create :team_win => params[:kick_at_goal_winner], :pronostic_id => pronostic_id
	            	        if pronostic_saved
		            	        flash[:success] = "Pronostic effectue !"
		            	    else
		            	        flash[:error] = "Erreur lors de l'enregistrement"
		            	    end
	            	    else
	            	        flash[:error] = "Erreur lors de l'enregistrement"
	            	    end
        		    else
        		    	new_pronostic = Pronostic.new(new_pronostic_params)
            	    	pronostic_saved = new_pronostic.save
	            	    if pronostic_saved
	            	        flash[:success] = "Pronostic effectue !"
	            	    else
	            	        flash[:error] = "Erreur lors de l'enregistrement"
	            	    end
					end
        	    else
        	        flash[:error] = "Vous avez deja etabli votre pronostic"
        	    end
        	end
        else
            flash[:error] = "Vous ne pouvez plus faire de pronostic a moins d'une heure"
        end
    	cookies[:matchday] = params[:matchday]

    	sync = Synchronizer.new
		synch_data = sync.get_rounds_and_games_with_kicks_at_goal
		@rounds = synch_data["rounds"]
		@games_with_kicks_at_goal = synch_data["gamesWithKicksAtGoal"]
		@title_page = "Mes pronostics - FIFA World Cup 2K14"

    	render :index
	end
end