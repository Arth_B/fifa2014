class ApplicationController < ActionController::Base
    # Prevent CSRF attacks by raising an exception.
    # For APIs, you may want to use :null_session instead.
    protect_from_forgery with: :exception
    helper_method :isAbleToCommitPronostic?, :is_signed_in?
    
    protected
    
    def authentificated!
        if session[:user]
            @current_user = {:id=>session[:user][:id], :first_name=>session[:user][:first_name], :last_name=>session[:user][:last_name]}
            return true	
        else
            flash[:error]="Authentifiez-vous avant !"
            redirect_to root_path
            return false
        end
    end
    
    def is_signed_in?
        session[:user] ? true : false
    end
    
    def get_session
        if session[:user]
            @current_user = {:id=>session[:user][:id], :first_name=>session[:user][:first_name], :last_name=>session[:user][:last_name]}
        end
    end
    
    def isAbleToCommitPronostic? game
        time_now_BRA = Time.zone.now.in_time_zone("America/Sao_Paulo")
    	game_time_BRA = game.datetime.in_time_zone("America/Sao_Paulo")
    	if ((game_time_BRA - time_now_BRA) / 1.hour).floor < 1
    	    false
    	else
    	    true
    	end
    end
end
