class Api::UsersController < ApiController
    def index
		@users = User.all
	end

	def show
	    first_name = params[:first_name].strip
	    last_name = params[:last_name].strip
	    user = User.where('first_name ILIKE ? AND last_name ILIKE ?', first_name, last_name)
	    returned_json = {}
	    if user.count==0
	        returned_json[:user_exits] = false
        else
            returned_json[:user_exits] = true
        end
	    render :json => returned_json
	end

	def new
		@user = User.new
	end

	def create
		new_user_params = params.required :user
		new_user_params.permit!

		new_user_params[:pin] = new_user_params[:pin].to_i

        existing_user = User.where('first_name ILIKE ? AND last_name ILIKE ?', new_user_params[:first_name].strip, new_user_params[:last_name].strip)
        if(existing_user.count==0)
    		new_user = User.new(new_user_params)
    		user_saved = new_user.save
    		if user_saved
    		  flash[:success]="Vous vous etes bien enregistre"
    		  user = {:id => new_user.id, :first_name => new_user_params[:first_name].strip, :last_name => new_user_params[:last_name].strip}
    		  session[:user] = user
    		  redirect_to mes_pronostiques_path
    		else
    		  flash[:error]="Erreur lors de l'enregistrement !"
    		  redirect_to root_path
    		end
    	else
    	  existing_user = existing_user.first
    	  if(new_user_params[:pin]==existing_user.pin)
    	      user = {:id => existing_user.id, :first_name => existing_user.first_name, :last_name => existing_user.last_name}
    	      session[:user] = user
    	      redirect_to mes_pronostiques_path
    	  else
    	      flash[:error]="Identifiants incorrects !"
    		  redirect_to root_path
          end
    	end

	end

	def edit
		@user = User.find(params[:id])
	end

	def update
		user_params = params.required :user
		user_params.permit!
		current_user = User.find(user_params[:id].to_i)
		current_user.update_attributes(user_params)
		flash[:success]="L'utilisateur a bien ete modifie"
		redirect_to mes_pronostiques_path
	end

	def destroy
		@user = User.find(params[:id])
		@user.destroy
		flash[:success]="L'utilisateur a bien ete supprime"
		redirect_to root_path
	end
end