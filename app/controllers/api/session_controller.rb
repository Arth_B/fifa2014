class Api::SessionController < ApiController
    def destroy
        reset_session
        flash[:success]="Vous etes deconnectes !"
        redirect_to root_path
    end
end