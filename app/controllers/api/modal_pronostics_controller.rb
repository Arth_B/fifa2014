class Api::ModalPronosticsController < ApiController
    before_filter :authentificated!
    
    def index
	end
	
	def show
		@users = User.all.order('last_name ASC')
		@game = Game.find params[:id]
		@game_id = params[:id]
	end

	def new
	end
	
	def create
	end

	def edit
	end

	def update
	end

	def destroy
	end
end