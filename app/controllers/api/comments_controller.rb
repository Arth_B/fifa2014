class Api::CommentsController < ApiController
    before_filter :authentificated!
    
    def index
	end
	
	def show
	end

	def new
	end
	
	def create
		new_comment_params = params.required :comment
		new_comment_params.permit!
		
		new_comment_params[:user_id] = session[:user][:id]
		if new_comment_params[:discussion_thread_id].nil?
		    constructed_json = {:status => 500, :error => "Missing discussion thread id" }
		    render :json => constructed_json, :status => :internal_server_error
		else
		    if DiscussionThread.find(new_comment_params[:discussion_thread_id]).nil?
		        constructed_json = {:status => 500, :error => "No discussion thread #{params[:id]} founded" }
                render :json => constructed_json, :status => :internal_server_error
		    else
            	new_comment = Comment.new(new_comment_params)
        		comment_saved = new_comment.save
        		if comment_saved
        		    constructed_json = {:status => 200, :message => "Commentaire envoye"}
        		    render :json => constructed_json
        		else
                    constructed_json = {:status => 500, :error => "No discussion thread #{params[:id]} founded" }
                    render :json => constructed_json, :status => :internal_server_error
        		end
		    end
		end
	end

	def edit
	end

	def update
	end

	def destroy
	end
end