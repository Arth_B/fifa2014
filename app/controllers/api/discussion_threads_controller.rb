class Api::DiscussionThreadsController < ApiController
    before_filter :authentificated!
    
    def index
        constructed_json = {}
        constructed_json[:discussion_threads] = []
		discussion_threads = DiscussionThread.all
		discussion_threads.each do |discussion_thread|
		    discussion_thread_json = {:name => discussion_thread.name, :messages => []}
		    discussion_thread_comments = discussion_thread.comments.order('created_at DESC')
		    discussion_thread_comments.each do |discussion_thread_comment|
		        discussion_thread_comment_user = discussion_thread_comment.user
		        discussion_thread_comment_json = {:user_id => discussion_thread_comment_user.id, :user_name => discussion_thread_comment_user.first_name.titleize+" "+discussion_thread_comment_user.last_name.upcase, :user_name_short => discussion_thread_comment_user.first_name.titleize+" "+discussion_thread_comment_user.last_name.upcase[0]+".", :created_at => discussion_thread_comment.created_at.to_s, :message => discussion_thread_comment.message, :id => discussion_thread_comment.id }
    		    discussion_thread_json[:messages] << discussion_thread_comment_json
    		end
		    constructed_json[:discussion_threads] << discussion_thread_json
		end
		render :json => constructed_json
	end
	
	def show
	    discussion_thread = DiscussionThread.find params[:id]
	    if discussion_thread.nil?
	        constructed_json = {:status => 404, :error => "No discussion thread #{params[:id]} founded" }
	        render :json => constructed_json, :status => :not_found
	    else
	        constructed_json = {:name => discussion_thread.name, :messages => []}
	        discussion_thread_comments = discussion_thread.comments.order('created_at DESC')
	        discussion_thread_comments.each do |discussion_thread_comment|
		        discussion_thread_comment_user = discussion_thread_comment.user
		        discussion_thread_comment_json = {:user_id => discussion_thread_comment_user.id, :user_name => discussion_thread_comment_user.first_name.titleize+" "+discussion_thread_comment_user.last_name.upcase, :user_name_short => discussion_thread_comment_user.first_name.titleize+" "+discussion_thread_comment_user.last_name.upcase[0]+".", :created_at => discussion_thread_comment.created_at.to_s, :message => discussion_thread_comment.message, :id => discussion_thread_comment.id }
    		    constructed_json[:messages] << discussion_thread_comment_json
    		end
		    render :json => constructed_json
	    end
	end

	def new
	end

	def create
	end

	def edit
	end

	def update
	end

	def destroy
	end
end