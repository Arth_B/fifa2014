module ApplicationHelper
  # Flash
  def flash_level(level)
    case level
    when :success then {"class"=>"success", "icon"=>"glyphicon glyphicon-thumbs-up"}
    when :notice then {"class"=>"success", "icon"=>"glyphicon glyphicon-thumbs-up"}
    when :error then {"class"=>"warning", "icon"=>"glyphicon glyphicon-warning-sign"}
    when :danger then {"class"=>"danger", "icon"=>"glyphicon glyphicon-warning-sign"}
    when :alert then {"class"=>"warning", "icon"=>"glyphicon glyphicon-warning-sign"}
    else {"class"=>"info", "icon"=>"glyphicon glyphicon-info-sign"}
    end
  end
end
