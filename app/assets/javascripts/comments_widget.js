// CLASS Comment
function Comment() {
    Comment.apiUrl = "/api/comments";
    if (typeof Comment.initialized===undefined) {
        Comment.message = "";
        Comment.prototype.new = function(message, discussionThreadId) {
            Comment.message = message;
            Comment.discussionThreadId = discussionThreadId;
        };
        Comment.prototype.save = function() {
            dataToSend = {
                'comment[message]': Comment.message,
                'comment[discussion_thread_id]': Comment.discussionThreadId
            };
            if(Comment.discussionThreadId!==undefined) {
                $.ajax({
                    type: 'POST',
                    url: Comment.apiUrl,
                    dataType: 'json',
                    data: dataToSend,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function(returnData) {
                        debugger;
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        debugger;
                    }
                });
            }
        };
        Comment.initialized = true;
    }
}

function CommentsWidget() {
    if (typeof CommentWidget.initialized ===undefined) {
        CommentsWidget.open = false;
        
        CommentWidget.prototype.bounce = function() {
            
        };
    };
    CommentWidget.initialized = true;
}

/**
 *  EXECUTE
 */
/*var comment = new Comment();
comment.new("Test", 1);
comment.save();*/