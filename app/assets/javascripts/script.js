/*******************************************************
 ********************** GLOBAL VARS ********************
 *******************************************************/
window.createAccount = false;

/*******************************************************
 *********************** FUNCTIONS *********************
 *******************************************************/

/**
 *  Element undefined?
 */
function isUndefined(element) {
    return element===undefined ? true : false;
}

/**
 *	Set sections height for homepage items
 */
function setSectionsHeight() {
	var windowHeight = $(window).innerHeight();
	if (currentPage == "accueil") {
		$("#section-accueil").height(windowHeight);
		$("#section-mes-pronostiques-auth").height(windowHeight);
	} else if (currentPage == "mes-pronostiques") {
		var $container = $("#section-mes-pronostiques");
		if ($container.height() < windowHeight)
			$container.height(windowHeight);
	} else if (currentPage == "le-classement") {
		var $container = $("#section-le-classement");
		if ($container.height() < windowHeight)
			$container.height(windowHeight);
	}
}

/**
 *	getDocHeight
 */
function getDocHeight() {
	var D = document;
	return Math.max(
		D.body.scrollHeight, D.documentElement.scrollHeight,
		D.body.offsetHeight, D.documentElement.offsetHeight,
		D.body.clientHeight, D.documentElement.clientHeight
	);
}

/**
 *	scrollToBottom
 */
function scrollToBottom(callback) {
	$("html, body").stop().animate({
			scrollTop: $document.height()
		},
		600,
		function() {
			if (callback)
				callback();
		}
	);
}

/**
 *	scrollToElt
 */
function scrollToElt(elt) {
	var scrollToVal = getDocHeight() - $(elt).offset().top;
	$("html, body").stop().animate({
			scrollTop: scrollToVal
		},
		600
	);
}

/**
 *	scrollToEltPronostic
 */
function scrollToEltPronostic(elt) {
	var scrollToVal = $(elt).offset().top - 155;
	$("html, body").stop().animate({
			scrollTop: scrollToVal
		},
		600
	);
}

/**
 *	Autoscroll on homepage
 */
function autoScrollOnHomepage(scrollPos, scrollDir) {
	if (currentPage == "accueil") {
		if (autoScrollLauched === false && scrollDir == "down") {
			if (scrollPos > 200) {
				autoScrollLauched = true;
				scrollToBottom(function() {
					autoScrollLauched = false;
				});
			}
		}
	}
}

/**
 *	Get scroll direction
 */
function getScrollDirection(currentScrollPos) {
	var scrollDir = (previousScrollPos < currentScrollPos) ? "down" : "up";
	previousScrollPos = currentScrollPos;
	return scrollDir;
}

/**
 *	Set backgrounds full sized
 */
function setBackgroundsFullSize() {
	if (currentPage == "accueil") {
		$(".body-background-home-colored").backstretch($("#url-body-background-home-colored").text());
		$(".body-background-home-greyscale").backstretch($("#url-body-background-home-greyscale").text());
	} else if (currentPage == "mes-pronostiques") {
		$(".body-background-mes-pronostiques-colored").backstretch($("#url-body-background-mes-pronostiques-colored").text());
		$(".body-background-mes-pronostiques-greyscale").backstretch($("#url-body-background-mes-pronostiques-greyscale").text());
	} else if (currentPage == "le-classement") {
		$(".body-background-le-classement-colored").backstretch($("#url-body-background-le-classement-colored").text());
		$(".body-background-le-classement-greyscale").backstretch($("#url-body-background-le-classement-greyscale").text());
	}
}

/**
 *  Navbar mes-pronostics
 */
function toggleNavbarOnScroll(scrollPos) {
	if (currentPage == "mes-pronostiques") {
		var $topNav = $("#top-nav");
		var $topNavRounds = $("#top-nav #nav-rounds");
		var brTitle = $("#top-nav  p.title-primary > br");
		var $sectionPronostics = $('#section-mes-pronostiques');
		var $titleAndLinks = $("h1.text-blue");
		if (scrollPos > 115 && scrollPos <= 165) {
			$sectionPronostics.css('padding-top', '265px');
			$titleAndLinks.hide();
			brTitle.hide();
			$topNavRounds.show();
			$topNav.css({
				"background": "#FFF",
				"position": "fixed",
				"width": "100%",
				"left": "15px",
				"opacity": (scrollPos - 115) * 0.02,
				"top": 0
			});
		} else if (scrollPos > 165) {
			$sectionPronostics.css('padding-top', '265px');
			$titleAndLinks.hide();
			brTitle.hide();
			$topNavRounds.show();
			$topNav.css({
				"background": "#FFF",
				"position": "fixed",
				"width": "100%",
				"left": "15px",
				"opacity": 1,
				"top": 0
			});
		} else {
			$titleAndLinks.show();
			brTitle.show();
			$topNavRounds.hide();
			$sectionPronostics.removeAttr("style");
			$topNav.removeAttr("style");
		}
	}
}

/**
 *  Ranking toggle
 */
rankingUserSelected = "";

function rankingToggle() {
	if ($("#users-pronostics").length != 0) {
		var setTableMargin = function(that) {
			var marginTop = ($(that).index()) * 67;
			$("#section-le-classement #users-pronostics").css('margin-top', marginTop);
		};
		var resetTableMargin = function() {
			$("#section-le-classement #users-pronostics").css('margin-top', 0);
		};
		var $allElements = $("#section-le-classement #users-pronostics tr[class^='users-pronostics-for-']");

		$("#user-ranking tbody tr").mouseleave(function(event) {
			event.stopPropagation();
			if (rankingUserSelected == "") {
				resetTableMargin();
				$allElements.stop(true, true).fadeOut(200);
				$("#users-pronostics tbody tr.users-pronostics-default").stop(true, true).fadeIn(500);
				$("#user-ranking tbody tr").removeClass("user-selected");
			} else {
				setTableMargin($("#user-ranking tbody tr[data-target='" + rankingUserSelected + "']")[0]);
				var $userElements = $("#users-pronostics tbody tr." + rankingUserSelected);
				$allElements.stop(true, true).fadeOut(200);
				$("#users-pronostics tbody tr.users-pronostics-default").stop(true, true).fadeOut(200);
				$userElements.stop(true, true).fadeIn(500);
				$("#user-ranking tbody tr").removeClass("user-selected");
				$("#user-ranking tbody tr[data-target='" + rankingUserSelected + "']").addClass("user-selected");
			}
		});
		$("#user-ranking tbody tr").mouseenter(function(event) {
			event.stopPropagation();
			setTableMargin(this);
			var $allElements = $("#section-le-classement #users-pronostics tr[class^='users-pronostics-for-']");
			var $userElements = $("#users-pronostics tbody tr." + $(this).attr('data-target'));
			$allElements.stop(true, true).fadeOut(200);
			$("#users-pronostics tbody tr.users-pronostics-default").stop(true, true).fadeOut(200);
			$userElements.stop(true, true).fadeIn(500);
			$("#user-ranking tbody tr").removeClass("user-selected");
			$(this).addClass("user-selected");
		});
		$("#user-ranking tbody tr").click(function(event) {
			event.stopPropagation();
			if (rankingUserSelected == $(this).attr('data-target')) {
				resetTableMargin();
				$allElements.stop(true, true).fadeOut(200);
				$("#users-pronostics tbody tr.users-pronostics-default").stop(true, true).fadeIn(500);
				$("#user-ranking tbody tr").removeClass("user-selected");
				rankingUserSelected = "";
			} else if (!$(this).hasClass("user-selected")) {
				setTableMargin(this);
				var $userElements = $("#users-pronostics tbody tr." + $(this).attr('data-target'));
				rankingUserSelected = $(this).attr('data-target');
				$allElements.stop(true, true).fadeOut(200);
				$("#users-pronostics tbody tr.users-pronostics-default").stop(true, true).fadeOut(200);
				$userElements.stop(true, true).fadeIn(500);
				$("#user-ranking tbody tr").removeClass("user-selected");
				$(this).addClass("user-selected");
			} else {
				setTableMargin(this);
				rankingUserSelected == $(this).attr('data-target');
				rankingUserSelected = $(this).attr('data-target');
				$("#user-ranking tbody tr").removeClass("user-selected");
				$(this).addClass("user-selected");
			}
		});
	} else {
		$('#section-le-classement #user-ranking tbody tr').css('cursor', 'default');
	}
}

/**
 *  Modal toggle
 */
function modalToggle() {
	$("#section-mes-pronostiques .row-pool .col-pronostics .players-pronostics").click(function(event) {
		event.stopPropagation();
		if (!$('body').hasClass('modal-open')) {
			$('#' + $(this).attr("data-target")).modal('toggle');
		}
	});
}

/**
 *  Check inputs
 */
function checkInputs() {
	function checkAllInputs(that) {
		var target = $(that).parent().attr('id');
		var isWithKicksAtGoal = $(that).parent().hasClass("with-kicks-at-goal");
		if(isWithKicksAtGoal) {
			if(
				($(that).parent().find('input[name="kick_at_goal_winner"]').val()=="a" || $(that).parent().find('input[name="kick_at_goal_winner"]').val()=="b") &&
				($(that).parent().find('input[name="pronostic[team_a_score]"]').val()==$(that).parent().find('input[name="pronostic[team_b_score]"]').val()) &&
				($(that).parent().find('input[name="pronostic[team_a_score]"]').val().trim().length > 0 && $(that).parent().find('input[name="pronostic[team_b_score]"]').val().trim().length > 0)
			) {
				$('[data-validate=' + target + ']').attr('disabled', false).removeClass('disabled');
				$(that).parent().parent().parent().find('img').css("cursor", "pointer");
				$(that).parent().parent().parent().find('img').addClass('clickable');
				$(that).parent().find('span[data-toggle="popover"]').popover('hide');
			} else if (
				($(that).parent().find('input[name="pronostic[team_a_score]"]').val()!=$(that).parent().find('input[name="pronostic[team_b_score]"]').val()) &&
				($(that).parent().find('input[name="pronostic[team_a_score]"]').val().trim().length > 0 && $(that).parent().find('input[name="pronostic[team_b_score]"]').val().trim().length > 0)
			) {
				$('[data-validate=' + target + ']').attr('disabled', false).removeClass('disabled');
				$(that).parent().parent().parent().find('img').removeAttr('style');
				$(that).parent().parent().parent().find('img').css("cursor", "pointer");
				$(that).parent().parent().parent().find('img').removeClass('clickable').removeClass('flag-active');
				$(that).parent().find('span[data-toggle="popover"]').popover('hide');
				$(that).parent().find('input[name="kick_at_goal_winner"]').val("");
			} else if (
				($(that).parent().find('input[name="kick_at_goal_winner"]').val()!="a" && $(that).parent().find('input[name="kick_at_goal_winner"]').val()!="b") &&
				($(that).parent().find('input[name="pronostic[team_a_score]"]').val()==$(that).parent().find('input[name="pronostic[team_b_score]"]').val()) &&
				($(that).parent().find('input[name="pronostic[team_a_score]"]').val().trim().length > 0 && $(that).parent().find('input[name="pronostic[team_b_score]"]').val().trim().length > 0)
			) {
				$('[data-validate=' + target + ']').attr('disabled', true).addClass('disabled');
				$(that).parent().parent().parent().find('img').removeAttr('style');
				$(that).parent().parent().parent().find('img').css("cursor", "pointer");
				$(that).parent().parent().parent().find('img').addClass('clickable');
				$(that).parent().find('span[data-toggle="popover"]').popover('show');
				$(that).parent().find('input[name="kick_at_goal_winner"]').val("");
			} else {
				$('[data-validate=' + target + ']').attr('disabled', true).addClass('disabled');
				$(that).parent().find('span[data-toggle="popover"]').popover('hide');
				$(that).parent().parent().parent().find('img').removeAttr('style');
				$(that).parent().parent().parent().find('img').css("cursor", "default");
				$(that).parent().parent().parent().find('img').removeClass('clickable').removeClass('flag-active');
				$(that).parent().find('input[name="kick_at_goal_winner"]').val("");
			}
		} else {
			if (($(that).parent().find('input[name="pronostic[team_a_score]"]').val().trim().length > 0 && $(that).parent().find('input[name="pronostic[team_b_score]"]').val().trim().length > 0)) {
				$('[data-validate=' + target + ']').attr('disabled', false).removeClass('disabled');
			} else {
				$('[data-validate=' + target + ']').attr('disabled', true).addClass('disabled');
			}
		}
	}
	$('.row-pool img').click(function(event){
		event.stopPropagation();
		if($(this).hasClass("clickable")) {
			$(this).parent().parent().find('img').removeAttr('style').removeClass('flag-active');
			$(this).addClass('flag-active');
			var kick_at_goal_winner = $(this).attr('data-team');
			$(this).parent().parent().find('form.with-kicks-at-goal input[name="kick_at_goal_winner"]').val(kick_at_goal_winner);
			var that = $(this).parent().parent().find('form.with-kicks-at-goal input[name="pronostic[team_a_score]"]')[0]
			checkAllInputs(that);
		}
		return false;
	});
	$('.col-pronostics form input[type=number]').change(function(event) {
		event.stopPropagation();
		checkAllInputs(this);
	});
	$('.col-pronostics form input[type=number]').keyup(function(event) {
		event.stopPropagation();
		checkAllInputs(this);
	});
}

/*******************************************************
 *********************** EXECUTION *********************
 *******************************************************/
/*$(document).on("page:before-change", function(){
	debugger;
});
$(document).on("page:fetch", function(){
	debugger;
});
$(document).on("page:receive", function(){
	debugger;
});
$(document).on("page:change", function(){
	debugger;
});
$(document).on("page:update", function(){
	debugger;
});
$(document).on("page:load", function(){
	debugger;
});*/

$(document).ready(function() {

	/**
	 *	Define usefull vars
	 */
	$document = $(document);
	$window = $(window);
	currentPage = $("#var-current-page").text();
	previousScrollPos = 0;
	autoScrollLauched = false;

	setBackgroundsFullSize();

	$(document).on("page:change", function() {
		/**
		 * Activate tooltips
		 */
		$('[data-toggle=tooltip]').tooltip();

		/**
		 * User ranking toggle
		 */
		rankingToggle();

		/**
		 *  Modal toggle
		 */
		modalToggle();

		/**
		 *  Check inputs
		 */
		checkInputs();

		/**
		 *  Matchday scoll
		 */
		if ($.cookie('matchday')) {
			var matchdayId = "#" + $.cookie('matchday');
			$.removeCookie("matchday");
			scrollToEltPronostic(matchdayId);
		}
	});


	$window.load(function() {
		/**
		 *	Set some heights
		 */
		setSectionsHeight();

		/**
		 *	Resize sections if window's resized
		 */
		$window.resize(function() {
			setSectionsHeight();
		});

		if (currentPage == "accueil")
			$('.body-overlay-loading').fadeOut(1500);
	});

	/**
	 *	Bind scroll
	 */
	$window.scroll(function(event) {
		var scrollPos = $window.scrollTop();
		var scrollDir = getScrollDirection(scrollPos);

		autoScrollOnHomepage(scrollPos, scrollDir);
	});

	/**
	 *	Section : accueil
	 */
	over = false;
	$('#btn-mes-pronostiques').hover(function() {
		if (!over) {
			over = true;
			$('[class^=body-overlay-').hide();
			$('.body-background-home-colored').hide();
			$('.body-background-home-greyscale').show();
			$('.body-overlay-blue').show();
		}
	}, function() {
		if (over) {
			over = false;
			$('[class^=body-overlay-').hide();
			$('.body-background-home-greyscale').hide();
			$('.body-background-home-colored').show();
		}
	});

	$('#btn-le-classement').hover(function() {
		if (!over) {
			over = true;
			$('[class^=body-overlay-').hide();
			$('.body-background-home-colored').hide();
			$('.body-background-home-greyscale').show();
			$('.body-overlay-green').show();
		}
	}, function() {
		if (over) {
			over = false;
			$('[class^=body-overlay-').hide();
			$('.body-background-home-greyscale').hide();
			$('.body-background-home-colored').show();
		}
	});

	$('#btn-mes-pronostiques').click(function() {
		scrollToElt("#section-mes-pronostiques-auth");
	});

	$("#btn-le-classement").click(function() {
		window.location.href = "/le-classement";
	});

	$('#section-mes-pronostiques-auth form#user-auth button.hidden-xs').hover(function() {
		$(this).find('img').toggle();
	}, function() {
		$(this).find('img').toggle();
	});

	/**
	 *	Form submit
	 */
	$('#user-auth').submit(function(event) {
		var error = false;
		var $inputFirstName = $(this).find('input#user-first-name');
		var $inputLastName = $(this).find('input#user-last-name');
		var $inputPin = $(this).find('input#user-pin');
		$inputFirstName.parent().removeClass("has-error");
		$inputLastName.parent().removeClass("has-error");
		$inputPin.parent().removeClass("has-error");
		if ($inputFirstName.val().trim() == "") {
			error = true;
			$inputFirstName.parent().addClass("has-error");
		}
		if ($inputLastName.val().trim() == "") {
			error = true;
			$inputLastName.parent().addClass("has-error");
		}
		if ($inputPin.val().trim() == "") {
			error = true;
			$inputPin.parent().addClass("has-error");
		}
		if (!(/^\d{4}$/.test($inputPin.val()))) {
			error = true;
			$inputPin.parent().addClass("has-error");
		}
		if (error) {
			return false;
		} else {
		    if(createAccount===false) {
    		    var url = $(this).attr('action');
    			$.get(url, {
    				first_name: $inputFirstName.val().trim(),
    				last_name: $inputLastName.val().trim()
    			}, function(data){
    			    if(!data.user_exits) {
    			        $modal = $('#create-user');
    			        $modal.find('.modal-body').html("Bonjour <strong>"+$inputFirstName.val()+" "+$inputLastName.val()+"</strong>,<br>Est-ce votre première connexion ?<br>Le cas échéant, annulez et saisissez vos bons identifiants.");
    			        $modal.modal('show');
    			    } else {
    			        createAccount = true;
                	    $('#user-auth').submit();
    			    }
    			});
    			return false;
		    }
		}
	});
	
	$('#go-create-account').click(function(){
	    createAccount = true;
	    $('#user-auth').submit();
	});

	$('.submit-pronostic').click(function(event) {
		var submitForm = $(this).attr('data-validate');
		$('#' + submitForm).submit();
	});

	$("#nav-rounds li a").click(function(event) {
		scrollToEltPronostic($(this).attr('href'))
	});
});