class AddScoreAndRankToUsers < ActiveRecord::Migration
  def change
      add_column :users, :score, :integer, default: 0
      add_column :users, :rank, :integer, default: 999
  end
end
