class AddRatioGoodPronosticsToUsers < ActiveRecord::Migration
  def change
      add_column :users, :ratio_good_pronostics, :float, default: 0
  end
end
