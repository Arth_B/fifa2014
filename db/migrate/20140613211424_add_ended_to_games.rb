class AddEndedToGames < ActiveRecord::Migration
  def change
      add_column :games, :ended, :boolean
  end
end
