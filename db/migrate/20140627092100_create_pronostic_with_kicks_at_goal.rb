class CreatePronosticWithKicksAtGoal < ActiveRecord::Migration
  def change
    create_table :pronostic_with_kicks_at_goals do |t|
      t.string :team_win
      t.integer :score
      t.references :user
      t.references :pronostic
      t.timestamps
    end
  end
end
