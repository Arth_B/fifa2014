class CreateComment < ActiveRecord::Migration
  def change
    create_table :comments do |t|
        t.text :message
        t.references :discussion_thread
        t.references :user
        t.timestamps
    end
  end
end
