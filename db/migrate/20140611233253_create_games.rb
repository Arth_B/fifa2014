class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :team_a_country
      t.string :team_a_flag
      t.integer :team_a_score
      t.string :team_b_country
      t.string :team_b_flag
      t.integer :team_b_score
      t.timestamps
    end
  end
end
