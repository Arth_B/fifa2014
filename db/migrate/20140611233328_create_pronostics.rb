class CreatePronostics < ActiveRecord::Migration
  def change
    create_table :pronostics do |t|
      t.integer :team_a_score
      t.integer :team_b_score
      t.integer :score
      t.references :user
      t.references :game
      t.timestamps
    end
  end
end
