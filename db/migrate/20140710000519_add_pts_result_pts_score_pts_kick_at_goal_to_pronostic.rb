class AddPtsResultPtsScorePtsKickAtGoalToPronostic < ActiveRecord::Migration
  def change
  	add_column :pronostics, :pts_result, :integer, default: 0
  	add_column :pronostics, :pts_score, :integer, default: 0
  	add_column :pronostics, :pts_kick_at_goal, :integer, default: 0
  end
end
